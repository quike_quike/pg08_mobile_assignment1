package com.example.pg08carlos.exercise1;
/**
 * Created by Carlos Alavez on XXI Century.
 */
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class AddressActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        Intent intent = getIntent();
        String address = intent.getStringExtra("address");

        TextView addressTextView = (TextView) findViewById(R.id.addressTextView);
        addressTextView.setText(address);


    }
}
