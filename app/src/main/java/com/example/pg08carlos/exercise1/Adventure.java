package com.example.pg08carlos.exercise1;
/**
 * Created by Carlos Alavez on XXI Century.
 */
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Adventure extends AppCompatActivity {
    int page;
    Story adventure;
    TextView text;
    Button choice1,choice2,returnButton;
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        page=0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adventure);

        Intent intent = getIntent();
        String name = intent.getStringExtra("hero");
        adventure = new Story(name);

        text = (TextView)findViewById(R.id.storyText);
        image = (ImageView)findViewById(R.id.storyImage);
        choice1 = (Button)findViewById(R.id.choice1);
        choice2 = (Button)findViewById(R.id.choice2);
        returnButton = (Button)findViewById(R.id.returnButton);
        returnButton.setVisibility(View.GONE);

        text.setText(adventure.getPage(page).getText());
        image.setImageResource(adventure.getPage(page).getImageId());
        choice1.setText(adventure.getPage(page).getChoice1().getText());
        choice2.setText(adventure.getPage(page).getChoice2().getText());


    }

    public void onChoice1Click(View clickedButton){
        page=adventure.getPage(page).getChoice1().getNextPage();
        updatePage();
    }

    public void onChoice2Click(View clickedButton){
        page=adventure.getPage(page).getChoice2().getNextPage();
        updatePage();
    }

    public void onReturnClick(View clickedButton){
        page=0;
        Intent mainIntent = new Intent(this, MainActivity.class);
        //assert name != null;
        //mainIntent.putExtra("hero", name.getText().toString());
        this.startActivity(mainIntent);
    }

    public void updatePage(){
        text.setText(adventure.getPage(page).getText());
        image.setImageResource(adventure.getPage(page).getImageId());
        if(adventure.getPage(page).isFinal()){
            choice1.setVisibility(View.GONE);
            returnButton.setVisibility(View.VISIBLE);
            choice2.setVisibility(View.GONE);
        }else{
            choice1.setText(adventure.getPage(page).getChoice1().getText());
            choice2.setText(adventure.getPage(page).getChoice2().getText());
        }
    }
}
