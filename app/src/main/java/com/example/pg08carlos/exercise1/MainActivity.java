package com.example.pg08carlos.exercise1;
/**
 * Created by Carlos Alavez on XXI Century.
 */
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    EditText name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView image = (ImageView)findViewById(R.id.imageView);
        image.setImageResource(R.drawable.conan2);

        name = (EditText) findViewById(R.id.nameField);
        name.setText("Conan");
    }

    public void onButtonClick(View clickedButton){
        Intent adventureIntent = new Intent(this, Adventure.class);
        adventureIntent.putExtra("hero", name.getText().toString());
        this.startActivity(adventureIntent);

    }


}
